var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');


var app = express();

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

//express中的get和post实际上也是一种中间件，只不过没有next()
//用express的static方法中间件可以方便的提供静态资源目录文件
app.use(express.static(path.join(__dirname, 'bower_components')));
app.use(bodyParser());


//将路由当成中间件来使用，注意代码的先后就是中间件使用的先后顺序
app.use(require('./list.js'));

app.listen(1337, function(){
	console.log('ready on port 1337 with nodemon');
});

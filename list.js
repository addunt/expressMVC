var express = require('express');
var router = express.Router();


var list = [{name: 'gaobin'}, {name: 'peter'}, {name: 'addunt'}];

router.get('/', function(req, res){
	res.render('index', {
		text: 'MY Site',
		list: list,
	});
});

router.post('/add', function(req, res){
	var name = req.body.name;
	list.push({name: name});

	res.redirect('/');
});

module.exports = router;